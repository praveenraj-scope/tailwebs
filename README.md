**Rails 6 + Docker**

Command
`docker-compose build`
`docker-compose run web rake db:create` 
`docker-compose run web rake db:migrate`
`docker compose up`

OR

install ruby -v 3.01
install postgres
install npm/nodejs
then

`bundle install`
`npm install`
`rails db:create`
`rails db:migrate`
`rails s`
