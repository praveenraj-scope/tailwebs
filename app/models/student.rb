class Student < ApplicationRecord
    # Scopes
    default_scope {where(deleted_at: nil)}
    
    # Associations
    belongs_to :user

    # Validations
    validates :name, presence: true, uniqueness: {scope: [:user_id, :deleted_at]}
end
