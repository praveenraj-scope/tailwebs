class Mark < ApplicationRecord
    # Scopes
    default_scope {where(deleted_at: nil)}
    
    # Associations
    belongs_to :student
    belongs_to :subject

    # Validations
    validates :score, presence: true
    validates :subject_id,  uniqueness: {scope: [:student_id, :deleted_at]}
    validates :student_id,  uniqueness: {scope: [:subject_id, :deleted_at]}

    def full_rs
        {
            id: id, student_name: student.name, subject: subject.name, score: score 
        }
    end
end
