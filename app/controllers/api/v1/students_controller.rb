class Api::V1::StudentsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_student, only: [:edit, :update, :show, :destroy]

    # GET /students index.html.erb
    def index
        where_condition = []
        where_condition << "student.name ILIKE '%#{params[:search_by]}%'" if params[:search_by].present?
        @pagy, @students = pagy(Student.where(user: current_user).where(where_condition.join(' AND ')), items: 10)
    end

    # GET /students/new new.html.erb
    def new
        @student = Student.new
    end

    # POST /students
    def create
        @student = Student.new(permit_params)
        @student.user = current_user
        respond_to do |format|
            if @student.save
                format.html { redirect_to api_v1_students_path, notice: "student was successfully created." }
                format.json { render :show, status: :created, location: @student }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @student.errors, status: :unprocessable_entity }
            end
        end
    end

    # GET /students/:id edit.html.erb
    def edit; end

    # PUT /students/:id 
    def update
        respond_to do |format|
            if @student.update(permit_params)
                format.html { redirect_to api_v1_students_path, notice: "student was successfully updated." }
                format.json { render :show, status: :created, location: @student }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @student.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /students/:id 
    def destroy
        @student.update!(deleted_at: DateTime.now)
        redirect_to api_v1_students_path
    end

    private

    def set_student
        p @student = current_user.students.find(params[:id])
    end

    def permit_params
        params.require(:student).permit(:name)
    end
end
