class Api::V1::SubjectsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_subject, only: [:edit, :update, :show, :destroy]

      # GET /subjects index.html.erb
    def index
        where_condition = []
        where_condition << "sujects.name ILIKE '%#{params[:search_by]}%'" if params[:search_by].present?
        @pagy, @subjects = pagy(Subject.where(user: current_user).where(where_condition.join(' AND ')), items: 10)
    end

    # GET /subjects/new new.html.erb
    def new
        @subject = Subject.new
    end

    # POST /subjects
    def create
        @subject = Subject.new(permit_params)
        @subject.user = current_user
        respond_to do |format|
            if @subject.save
                format.html { redirect_to api_v1_subjects_path, notice: "subject was successfully created." }
                format.json { render :show, status: :created, location: @subject }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @subject.errors, status: :unprocessable_entity }
            end
        end
    end

    # GET /subjects/:id edit.html.erb
    def edit; end

    # PUT /subjects/:id 
    def update
        respond_to do |format|
            if @subject.update(permit_params)
                format.html { redirect_to api_v1_subjects_path, notice: "subject was successfully updated." }
                format.json { render :show, status: :created, location: @subject }
            else
                format.html { render :new, status: :unprocessable_entity }
                format.json { render json: @subject.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /subjects/:id 
    def destroy
        @subject.update!(deleted_at: DateTime.now)
        redirect_to api_v1_subjects_path
    end

  private

  def set_subject
    @subject = current_user.subjects.find(params[:id])
  end

  def permit_params
    params.require(:subject).permit(:name)
  end
end
