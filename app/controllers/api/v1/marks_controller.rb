class Api::V1::MarksController < ApplicationController
    before_action :authenticate_user!   
    before_action :set_mark, only: [:edit, :update, :destroy]
    before_action :set_form_select, only: [:new, :edit,:create, :update]

    # GET /marks index.html.erb
    def index
        where_condition = []
        where_condition << "students.user_id = #{current_user.id} AND subjects.user_id = #{current_user.id}"
        where_condition << "subject_id = #{params[:subject_id]}" if params[:subject_id].present?
        where_condition << "student_id = #{params[:student_id]}" if params[:student_id].present?
        @pagy, @marks = pagy(Mark.joins(:student, :subject).where(where_condition.join(' AND ')), items: 10)
    end


    # GET /marks/:id/edit edit.htmle.rb
    def edit; end

    # PUT /marks/:id
    def update
        respond_to do |format|
            if @mark.update(score: params[:score])
                format.html { redirect_to api_v1_marks_path, notice: "Mark was successfully updated." }
            else
                format.html { render :edit, status: :unprocessable_entity }
            end
        end
    end

    # GET /marks/new new.html.erb
    def new
        @mark = Mark.new
    end
    
    # POST /marks
    def create
        @mark = Mark.find_by(student_id: params[:student_id], subject_id: params[:subject_id])
        if @mark.present?
            score = @mark.score + params[:score].to_i
            respond_to do |format|
                if @mark.update(score: score)
                    format.html { redirect_to api_v1_marks_path, notice: "Mark was successfully Updated." }
                else
                    format.html { render :new, status: :unprocessable_entity }
                end
            end
        else
           @mark = Mark.new(student_id: params[:student_id], subject_id: params[:subject_id], score: params[:score])
           respond_to do |format|
                if @mark.valid?
                    @mark.save
                    format.html { redirect_to api_v1_marks_path, notice: "Mark was successfully created." }
                else
                    debugger
                    format.html { render :new, status: :unprocessable_entity }
                end
            end
        end
    end

    # DELETE /marks/:id
    def destroy
        @mark.update(deleted_at: DateTime.now)
        redirect_to api_v1_marks_path
    end

    private

    def set_mark
        @mark = Mark.find(params[:id])
    end

    def set_form_select
        @students = current_user.students
        @subjects = current_user.subjects
    end
end
