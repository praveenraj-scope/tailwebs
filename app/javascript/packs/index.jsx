// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import {render} from 'react-dom';
import App from '../components/App'
// import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import listStudents from '../components/students/listStudents';
document.addEventListener("DOMContentLoaded", () => {
  render(
  //   <Router>
  //   <Switch>
  //     <Route path="/" exact component={listStudents} />
  //   </Switch>
  // </Router>,
  <App />
    , document.body.appendChild(document.createElement("div")));
});
