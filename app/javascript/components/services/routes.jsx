import React from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import {getUser} from './localStorage';
import listStudents from '../students/listStudents';
import listSubjects from '../subjects/listSubjects';
import studentMarks from '../marks/studentMarks';
import signup from '../authentication/signup';
import login from '../authentication/login';

export default function PublicRoutes() {
    const user = getUser()
    const RedirectToLogin = ({ component: Component, ...rest }) => {
        return (
            <Route
                {...rest}
                render={(props) =>
                    localStorage.getItem('user') ? (
                        <Component {...props} />
                    ) : (
                            window.location.href = '/users-login'
                        )}
            />
        );
    };
    console.log('some');
    return (
        <BrowserRouter>
            <Switch>
                <RedirectToLogin path="/" exact component={listStudents} />
                <RedirectToLogin path="/subjects-list" exact component={listSubjects} />
                <RedirectToLogin path="/students-marks" exact component={studentMarks} />
                <Route path="/users-login" component={signup} />
                <Route path="/users-signup" component={login} />
            </Switch>
        </BrowserRouter>
    )
}
