import axios from 'axios';
import { getToken } from './localStorage';
export default axios.create({
    baseURL: `http://127.0.0.1:3000/`,
    headers: {
        'Content-Type': 'application/json',
        Authorization: getToken(),
      },
    timeout: 10000,
  });  
