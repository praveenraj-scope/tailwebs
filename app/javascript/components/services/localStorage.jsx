
export const getUser = () => {
    const user = localStorage.getItem('user')
    if(user) {
        return JSON.parse(user).user;
    }
}

export const getToken = () => {
    return localStorage.getItem('token');
}

export const setUserSession = (token, user) => {
    const u = {user: user}
    localStorage.setItem("token", token);
    localStorage.setItem('user', JSON.stringify(u));
}

export const removeUserSession = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
}
